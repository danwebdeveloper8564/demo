﻿namespace DAL
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Configuration;
    
    using Microsoft.ApplicationBlocks.Data;
    using System.Xml;
    using System.IO;

    public sealed class DbConnectionDAL
    {
        // public static string sqlConnectionstring = ConfigurationManager.AppSettings["ConnectionString"].ToString();
        public static string sqlConnectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        //static SqlConnection cn = new SqlConnection(sqlConnectionstring);
        private static SqlParameter[] ConvertToSqlParameter(DbParameter[] dbParam)
        {
            return DbParameter.GetSqlParameter(dbParam);
        }

        public static void ExecuteNonQuery(CommandType cmdType, string cmdText)
        {
            SqlHelper.ExecuteNonQuery(sqlConnectionstring, cmdType, cmdText);
        }

        public static void ExecuteNonQuery(CommandType cmdType, string cmdText, DbParameter[] dbParam)
        {
            SqlParameter[] spParam = ConvertToSqlParameter(dbParam);
            SqlHelper.ExecuteNonQuery(sqlConnectionstring, cmdType, cmdText, spParam);
            DbParameter.SetDbParameterValueFromSql(spParam, dbParam);
        }

        public static SqlDataReader GetDataReader(CommandType cmdType, string cmdText)
        {
            return SqlHelper.ExecuteReader(sqlConnectionstring, cmdType, cmdText);
        }

        public static SqlDataReader GetDataReader(CommandType cmdType, string cmdText, DbParameter[] dbParam)
        {
            SqlParameter[] spParam = ConvertToSqlParameter(dbParam);
            SqlDataReader dReader = SqlHelper.ExecuteReader(sqlConnectionstring, cmdType, cmdText, spParam);
            DbParameter.SetDbParameterValueFromSql(spParam, dbParam);
            return dReader;
        }

        public static DataSet GetDataSet(CommandType cmdType, string cmdText)
        {
            return SqlHelper.ExecuteDataset(sqlConnectionstring, cmdType, cmdText);
        }

        public static DataSet GetDataSet(CommandType cmdType, string cmdText, DbParameter[] dbParam)
        {
            SqlParameter[] spParam = ConvertToSqlParameter(dbParam);
            DataSet dsetTable = SqlHelper.ExecuteDataset(sqlConnectionstring, cmdType, cmdText, spParam);
            DbParameter.SetDbParameterValueFromSql(spParam, dbParam);
            return dsetTable;
        }

        public static DataTable GetDataTable(CommandType cmdType, string cmdText)
        {
            return SqlHelper.ExecuteDataTable(sqlConnectionstring, cmdType, cmdText);
        }

        public static DataTable GetDataTable(CommandType cmdType, string cmdText, DbParameter[] dbParam)
        {
            SqlParameter[] spParam = ConvertToSqlParameter(dbParam);
            DataTable dtblTable = SqlHelper.ExecuteDataTable(sqlConnectionstring, cmdType, cmdText, spParam);
            DbParameter.SetDbParameterValueFromSql(spParam, dbParam);
            return dtblTable;
        }

        public static object GetScalarValue(CommandType cmdType, string cmdText)
        {
            return SqlHelper.ExecuteScalar(sqlConnectionstring, cmdType, cmdText);
        }
        public static int ExecuteQuery(string command)
        {
            int msg = 0;

            SqlConnection cn = new SqlConnection(sqlConnectionstring);
            SqlCommand cmd = new SqlCommand(command, cn);
            cmd.CommandText = command;
            cn.Open();
            try
            {
                msg = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            cn.Close();
            cmd = null;
            cn = null;
            return msg;
        }
        public static object GetScalarValue(CommandType cmdType, string cmdText, DbParameter[] dbParam)
        {
            SqlParameter[] spParam = ConvertToSqlParameter(dbParam);
            object objValue = SqlHelper.ExecuteScalar(sqlConnectionstring, cmdType, cmdText, spParam);
            DbParameter.SetDbParameterValueFromSql(spParam, dbParam);
            return objValue;
        }
        public static XmlDocument getXML(string Query, string tblnm, string dsnm)
        {
            DataTable dst = getFromDataTable(Query);
            DataSet dds = new DataSet();
            dds.Tables.Add(dst);
            dds.Tables[0].TableName = tblnm;
            dds.DataSetName = dsnm;
            StringWriter sw = new StringWriter();
            XmlTextWriter xtw = new XmlTextWriter(sw);
            XmlDocument xd = new XmlDocument();
            dds.WriteXml(xtw, XmlWriteMode.IgnoreSchema);
            string str = sw.ToString();
            xd.LoadXml(str);

            XmlNode nDate = xd.CreateElement("DateTime");
            nDate.InnerText = (DateTime.Now.Subtract(Convert.ToDateTime("01/01/1980"))).TotalMilliseconds.ToString();
            //nDate.Value = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
            XmlElement y = xd.DocumentElement;
            XmlNode x = xd.GetElementsByTagName(tblnm)[0];
            y.InsertBefore(nDate, x);

            //XmlNode y = xd.GetElementsByTagName(dsnm)[0].ChildNodes[1];
            //y.InnerText = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");

            sw.Close();
            xtw.Close();
            dds.Clear();
            dst.Clear();
            return xd;
        }
        public static DataTable getFromDataTable(string command)
        {
            SqlConnection con = new SqlConnection(sqlConnectionstring);
            SqlDataAdapter da = new SqlDataAdapter(command, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

    }
}

