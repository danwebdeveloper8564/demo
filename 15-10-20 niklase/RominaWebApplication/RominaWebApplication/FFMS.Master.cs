﻿using System;
using System.Text;
using System.Web.UI;
using System.Data;
using DAL;
using BusinessLayer;

namespace RominaWebApplication
{
    public partial class FFMS : System.Web.UI.MasterPage
    {
         string roleType = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["user_name"] != null)
                {
                    FillFY();
                    ddlFY.SelectedValue = Settings.Instance.FY;
                    
                    
                    lblSession.Text = Settings.Instance.FY;
                    LoadSideBar();
                    //Label4.DataBind();
                    roleType = Settings.Instance.RoleType;
                  //  userNameLabel.Text = Settings.Instance.UserName; Label4.Text = Settings.Instance.EmpName; lidist.Visible = true; 
                }
                else
                {
                    Response.Redirect("~/LogIn.aspx", true);
                }
            }
            //CheckNotifications();
        }

        private void FillFY()
        {
            string str = "select * from Financial_Year order by Financial_Year desc";
            DataTable dt = DbConnectionDAL.GetDataTable(CommandType.Text, str);
            if (dt.Rows.Count > 0)
            {
                ddlFY.DataSource = dt;
                ddlFY.DataTextField = "Financial_Year";
                ddlFY.DataValueField = "Financial_Year";
                ddlFY.DataBind();
            }
            ddlFY.SelectedValue = Settings.Instance.FY;
        }

        private string GetRoleType(string p)
        {
            try
            {
                string roleqry = @"select * from MastRole where RoleId=" + Convert.ToInt32(p) + "";
                DataTable roledt = DbConnectionDAL.GetDataTable(CommandType.Text, roleqry);
                return roledt.Rows[0]["RoleType"].ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return string.Empty;
            }
        }
        public string setClass(int Status)
        {
            string classToApply = string.Empty;
            if (Status == 0)
            {
                classToApply = "messagelabel";
            }
            else
            {
                classToApply = "messagelabel1";
            }

            return classToApply;

        }
        private void LoadSideBar()
        {
            string str = "select PageId,DisplayName,[Parent_Id] from MastPage where DisplayYN = 1 and  Parent_Id=0 order by Parent_Id,Level_Idx,Idx asc";
            string str1 = 
                     @"SELECT [MastPage].[PageId],[PageName],[Module],[DisplayName],[DisplayYN],[Img] ,[Parent_Id],[Level_Idx],[Idx] FROM [MastPage]
                         inner join MastRolePermission on MastPage.[PageId]=MastRolePermission.[PageId] where DisplayYN = 1 and
                         Parent_Id >-1 and RoleId='" + Settings.Instance.RoleID + "' and viewP=1 order by Parent_Id,Level_Idx,Idx asc";
            DataTable ParentDT = new DataTable();
            DataTable DtAll = new DataTable();
            ParentDT = DbConnectionDAL.GetDataTable(CommandType.Text, str);
            DtAll = DbConnectionDAL.GetDataTable(CommandType.Text, str1);
            if (ParentDT.Rows.Count > 0)
            {

                StringBuilder strMenu = new StringBuilder();
                strMenu.AppendLine();
                for (int i = 0; i < ParentDT.Rows.Count; i++)
                {
                    DataTable PagesDT = new DataTable();
                    DtAll.DefaultView.RowFilter = "Parent_Id='" + ParentDT.Rows[i]["PageId"].ToString() + "' ";
                    PagesDT = DtAll.DefaultView.ToTable();
                    if (PagesDT.Rows.Count > 0)
                    {
                        strMenu.AppendLine("<li class=\"treeview\"><a href=\"#\" " + (ParentDT.Select("Parent_Id = " + Convert.ToString(ParentDT.Rows[i]["PageId"])).Length > 0 ? "rel=\"" + Convert.ToString(ParentDT.Rows[i]["PageId"]) : "") +
                  "\"><i class=\"fa fa-th\"></i><span>" + ParentDT.Rows[i]["DisplayName"] + " </span><i class=\"fa fa-angle-left pull-right\"></i></a>");
                        strMenu.AppendLine("<ul class=\"treeview-menu\">");

                        for (int j = 0; j < PagesDT.Rows.Count; j++)
                        {
                            strMenu.AppendLine("<li><a href=\"" + (String.IsNullOrEmpty(Convert.ToString(PagesDT.Rows[j]["PageName"])) ? "" : ResolveUrl(Convert.ToString("~/" + PagesDT.Rows[j]["PageName"])) + "\" ") + "><i class=\"fa fa-circle-o\"></i>" + Convert.ToString(PagesDT.Rows[j]["DisplayName"]) + "</a></li>");
                        }
                        strMenu.AppendLine("</ul>");
                        strMenu.AppendLine("</li>");

                    }


                }
                strMenu.AppendLine("<ul class=\"treeview-menu\">");
                strMenu.AppendLine("</ul>");
                strMenu.AppendLine("</li>");
                strMenu.AppendLine("</ul>");
                LSidebar.InnerHtml = strMenu.ToString();
            }
        }
        protected void ddlFY_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Instance.FY = ddlFY.SelectedValue;
            lblSession.Text = Settings.Instance.FY;
            
            Response.Redirect(Request.Url.AbsoluteUri);
        }
    }
}