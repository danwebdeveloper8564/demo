﻿using BusinessLayer;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RominaWebApplication
{
    public partial class CompanyInformation : System.Web.UI.Page
    {
        string parameter = "";
        BAL.CompanyInformation.CompanyInformationBAL CI = new BAL.CompanyInformation.CompanyInformationBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            //-----------------------------//
            string pageName = Path.GetFileName(Request.Path);
            string PermAll = Settings.Instance.CheckPagePermissions(pageName, Convert.ToString(Session["user_name"]));
            string[] SplitPerm = PermAll.Split(',');
            if (btnSave.Text == "Save")
            {
                btnSave.Enabled = Convert.ToBoolean(SplitPerm[1]);
                btnSave.CssClass = "btn btn-primary";
            }
            else
            {
                btnSave.Enabled = Convert.ToBoolean(SplitPerm[2]);
                btnSave.CssClass = "btn btn-primary";
            }
            btnDelete.Enabled = Convert.ToBoolean(SplitPerm[3]);
            btnDelete.CssClass = "btn btn-primary";
            //-----------------------------//
            if (!IsPostBack)
            {
                FillControls(Convert.ToString(ViewState["ID"]));
                btnDelete.Visible = false;
            }
        }

        private void FillControls(string CompanyID)
        {
            try
            {

                string str = @"select * from Company_Information";
                DataTable Locdt = DbConnectionDAL.GetDataTable(CommandType.Text, str);
                if (Locdt.Rows.Count > 0)
                {
                    txtAddress1.Text = Locdt.Rows[0]["Address1"].ToString();
                    txtAddress2.Text = Locdt.Rows[0]["Address2"].ToString();
                    txtCity.Value = Locdt.Rows[0]["City"].ToString();
                    txtCompanyName.Value = Locdt.Rows[0]["Company_Name"].ToString();
                    txtCountry.Value = Locdt.Rows[0]["Country"].ToString();
                    txtEmail.Value = Locdt.Rows[0]["Email"].ToString();
                    txtFaxno.Value = Locdt.Rows[0]["Fax_No"].ToString();
                    txtPhoneno.Value = Locdt.Rows[0]["Phone_No"].ToString();
                    txtState.Value = Locdt.Rows[0]["State"].ToString();
                    hidCompanyID.Value = Locdt.Rows[0]["ID"].ToString();
                    txtSEmail.Value = Locdt.Rows[0]["SendingMail"].ToString();
                    txtPass.Value = Locdt.Rows[0]["Password"].ToString();
                    txtSMTP.Value = Locdt.Rows[0]["Server"].ToString();
                    txtPort.Value = Locdt.Rows[0]["Port"].ToString();

                    txtPBIUserName.Value = Locdt.Rows[0]["Bi_User_Name"].ToString();
                    txtPBIPassword.Value = Locdt.Rows[0]["Bi_Password"].ToString();

                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
               
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {}
        protected void btnFind_Click(object sender, EventArgs e)
        {}
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text == "Save")
            {
                int retsave = CI.Insert(0, txtCompanyName.Value, txtAddress1.Text, txtAddress2.Text, txtCity.Value, txtState.Value, txtCountry.Value, txtPhoneno.Value, txtFaxno.Value, txtEmail.Value,txtSMTP.Value,Convert.ToInt32(txtPort.Value),txtSEmail.Value,txtPass.Value,txtPBIUserName.Value,txtPBIPassword.Value);
                if (retsave == 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "Successmessage", "Successmessage('Record Inserted Successfully');", true);
                }
                else
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "errormessage", "errormessage('Record Cannot be Insert');", true);
                }
            }
            else
            {
                int retUpdate = CI.Update(Convert.ToInt32(hidCompanyID.Value), txtCompanyName.Value, txtAddress1.Text, txtAddress2.Text, txtCity.Value, txtState.Value, txtCountry.Value, txtPhoneno.Value, txtFaxno.Value, txtEmail.Value, txtSMTP.Value, Convert.ToInt32(txtPort.Value), txtSEmail.Value, txtPass.Value, txtPBIUserName.Value, txtPBIPassword.Value);
                if (retUpdate == 1)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "Successmessage", "Successmessage('Record Updated Successfully');", true);
                }
                else
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "errormessage", "errormessage('Record Cannot be Update');", true);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CompanyInformation.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int retdel = CI.delete(Convert.ToString(ViewState["ID"]));
                if (retdel == 1)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "Successmessage", "Successmessage('Record Deleted Successfully');", true);
                    btnDelete.Visible = false;
                    btnSave.Text = "Save";
                }
                else
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "errormessage", "errormessage('Record cannot be deleted as it is in use');", true);
                }

            }
        }
    }
}