﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/FFMS.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="DashBoard.aspx.cs" Inherits="RominaWebApplication.DashBoard" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .slicer-dropdown-popup { z-index: 100 !important; }

     
       
    </style>

    <script type="text/javascript">
        var V1 = "";
        function errormessage(V1) {
            $("#messageNotification").jqxNotification({
                width: 300, position: "top-right", opacity: 2,
                autoOpen: false, animationOpenDelay: 800, autoClose: true, autoCloseDelay: 3000, template: "error"
            });
            $('#<%=lblmasg.ClientID %>').html(V1);
             $("#messageNotification").jqxNotification("open");

         }
    </script>
    <script type="text/javascript">
        var V = "";
        function Successmessage(V) {
            $("#messageNotification").jqxNotification({
                width: 300, position: "top-right", opacity: 2,
                autoOpen: false, animationOpenDelay: 800, autoClose: true, autoCloseDelay: 3000, template: "success"
            });
            $('#<%=lblmasg.ClientID %>').html(V);
            $("#messageNotification").jqxNotification("open");
        }
    </script>
    <script type="text/javascript" src="scripts/powerbi.js">
        
        
        <%----%>
    </script><!--Power BI API Library -->


    <meta charset="UTF-8" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.numeric.min.js"></script>

    <script type="text/javascript">     <!--Power BI Reports/Dashboards related javascript-->

        //Configure IFrame for the Report after you have an Access Token. See Default.aspx.cs to learn how to get an Access Token
        window.onload = function () {
            var accessToken = document.getElementById('ContentPlaceHolder1_accessToken').value;
            if (!accessToken || accessToken == "") {
                return;
            }
            var embedUrl = document.getElementById('ContentPlaceHolder1_hdn_EmbedURL').value;
            var reportId = document.getElementById('ContentPlaceHolder1_hdn_reportID').value;


            // Embed configuration used to describe the what and how to embed.
            // This object is used when calling powerbi.embed.
            // This also includes settings and options such as filters.
            // You can find more information at https://github.com/Microsoft/PowerBI-JavaScript/wiki/Embed-Configuration-Details.
            var config = {
                //type: 'tile',
                type: 'dashboard',
                //type: 'report',
                accessToken: accessToken,
                embedUrl: embedUrl,
                id: reportId,
                settings: {
                    filterPaneEnabled: true,
                    navContentPaneEnabled: true
                }
            };
             
            // Grab the reference to the div HTML element that will host the report.
            var reportContainer = document.getElementById('reportContainer');
            //var reportContainer = document.getElementById('Global');

            // Embed the report and display it within the div container.
            var report = powerbi.embed(reportContainer, config);
            
        };
    </script>
    

    <div id="messageNotification">
        <asp:Label ID="lblmasg" runat="server"></asp:Label>
    </div>

    <section class="content-header dashboard-style">
            <h1>DASHBOARD</h1>
    </section>

    <div class="nav-tabs-custom">

        <ul class="nav nav-tabs padding-left10" id="myTabs" runat="server">
            <li class="active" id="tab_bi" runat="server">
                <asp:DropDownList ID="ddlDashboards"  style="width:200px; font-weight:bold;height: 30px;font-size: 14px !important;" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDashboards_SelectedIndexChanged"></asp:DropDownList>
                <asp:HiddenField ID="accessToken" runat="server" />
                <asp:HiddenField ID="hdn_reportID" runat="server" />
                <asp:HiddenField ID="hdn_reportName" runat="server" />
                <asp:HiddenField ID="hdn_EmbedURL" runat="server" />
            </li>
            <li class="" id="tab_chart" runat="server" >
                <a href="#activity" data-toggle="tab" aria-expanded="true">Other</a>
            </li>
            <li></li>
        </ul>
        
        <div class="tab-content" id="myTabContent" runat="server">

            <div class="tab-pane active" id="Global" runat="server">
                <div class="position-relative">
                    <%--<img src="img/logo_know.png" class="img-rounded logo_know_style" style="width: 13vh;" alt="User Image"/>--%>
                    <div id="reportContainer"></div>
                </div>
            </div>

            <div class="tab-pane" id="activity" runat="server">
            </div>
        </div>

    </div>
</asp:Content>
