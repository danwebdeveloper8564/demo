﻿using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using BusinessLayer;
using System.Net;


namespace RominaWebApplication
{
    public partial class PowerBI_Controller : System.Web.UI.Page
    {
        string Username ;
        string Password ;


        private static readonly string AuthorityUrl = "https://login.windows.net/common/oauth2/authorize/";
        private static readonly string ResourceUrl = "https://analysis.windows.net/powerbi/api";
        //private static readonly string BaseUrl = "https://api.powerbi.com/beta/myorg/";
        private static readonly string BaseUrl = "https://api.powerbi.com/v1.0/myorg/";
        private static readonly string ApiUrl = "https://api.powerbi.com/";
        private static readonly string ClientId = "5276122b-c46b-4f2c-a74d-c2da5e6d9d5a";
        private static readonly string ClientSecretId = "9kIgAiH26jZvzP1L.Qe.13FoS7.6_6Y6JV";
        protected void Page_Load(object sender, EventArgs e)
        {
            Username = Settings.Instance.PBI_UserName;
            Password = Settings.Instance.PBI_Password;
            if (Username == "" || Password == "")
            {
                show_ErrorMessgae("Power BI Credetials Not Found! Please provide the Admin Power BI Credentials in Company Information to get the Dashboards.");
                return;
            }
            
            bool exceptionOccured = false;
            try
            {
                var task = Task.Run(async () => { await GetAccessToken(); });
                task.Wait();
                if (Request.QueryString["interfaceID"]!= null)
                {

                    if (Request.QueryString["interfaceID"].ToString() == "Report")
                        Session["Report_Object_List"] = Get_Reports();
                    else if (Request.QueryString["interfaceID"].ToString() == "Dash")
                        Session["Dashboards_Object_List"] = Get_Dashboards();
                    else
                    {
                    
                    }
                }
                
            }
            catch (Exception ex)
            {
                if (ex.InnerException.ToString().Contains("The user or administrator has not consented to use the application"))
                    show_ErrorMessgae(ex.Message + " Inner Execption : The user or administrator has not consented to use the application. Please contatct your ADMINISTRATOR.");
                else
                    show_ErrorMessgae(ex.Message);
                exceptionOccured = true;
            }
            finally
            {
                if (!exceptionOccured)
                {
                    if (Request.QueryString["interfaceID"].ToString() == "Report")
                        Response.Redirect("~/Report.aspx");
                    else if (Request.QueryString["interfaceID"].ToString() == "Dash")
                        Response.Redirect("~/Dashboard.aspx");
                    else
                    { }
                    
                }
                    
            }
        }
        public async Task GetAccessToken()
        {
            try {
                //UserPasswordCredential credential1 = new UserPasswordCredential(Username, Password);
               // var authenticationContext = new AuthenticationContext(AuthorityUrl);
               // var authenticationResult = await authenticationContext.AcquireTokenAsync(ResourceUrl, ClientId, credential);
                //--block 
                // Authentication using app credentials
                var authenticationContext = new AuthenticationContext(AuthorityUrl);
                AuthenticationResult authenticationResult = null;
                UserPasswordCredential credential = new UserPasswordCredential(Username, Password);
                var credential1 = new ClientCredential(ClientId, ClientSecretId);
                authenticationResult = await authenticationContext.AcquireTokenAsync(ResourceUrl, credential1);
                Session["AccessToken"] = authenticationResult.AccessToken;
                return;
            }
            catch(Exception ex)
            {

            }
            
        }
        private PBIDashboards Get_Dashboards()
        {
            //Configure Reports request
            //System.Net.WebRequest request = System.Net.WebRequest.Create(String.Format("{0}/Reports", BaseUrl)) as System.Net.HttpWebRequest;
            System.Net.WebRequest request = System.Net.WebRequest.Create(String.Format("{0}/Dashboards", BaseUrl)) as System.Net.HttpWebRequest;

            request.Method = "GET";
            request.ContentLength = 0;
            //request.Headers.Add("Authorization", String.Format("Bearer {0}", accessToken.Value));
            request.Headers.Add("Authorization", String.Format("Bearer {0}", Session["AccessToken"].ToString()));

            //Get Report's response from request.GetResponse()
            using (var response = request.GetResponse() as System.Net.HttpWebResponse)
            {
                //Get reader from response stream
                using (var reader = new System.IO.StreamReader(response.GetResponseStream()))
                {
                    //Deserialize JSON string
                    PBIDashboards Dashboards = JsonConvert.DeserializeObject<PBIDashboards>(reader.ReadToEnd());
                    return Dashboards;
                }
            }
        }
        private PBIReports Get_Reports()
        {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //Configure Reports request
                System.Net.WebRequest request = System.Net.WebRequest.Create(String.Format("{0}/Reports", BaseUrl)) as System.Net.HttpWebRequest;

                request.Method = "GET";
                request.ContentLength = 0;
                //request.Headers.Add("Authorization", String.Format("Bearer {0}", accessToken.Value));

                var ss = Session["AccessToken"].ToString();
                request.Headers.Add("Authorization", String.Format("Bearer {0}", Session["AccessToken"].ToString()));
              //  request.UseDefaultCredentials = true;
                
            //add
                request.UseDefaultCredentials = true;

                request.PreAuthenticate = true;
            
                request.Credentials = new NetworkCredential("manoj.vishwakarma@dataman.in", "manoj@12345", "datamanin.onmicrosoft.com");//manoj account

                var response1 = request.GetResponse();
                //Get Report's response from request.GetResponse()
                using (var response = request.GetResponse() as System.Net.HttpWebResponse)
                {
                    //Get reader from response stream
                    using (var reader = new System.IO.StreamReader(response.GetResponseStream()))
                    {
                        //Deserialize JSON string
                        PBIReports reports = JsonConvert.DeserializeObject<PBIReports>(reader.ReadToEnd());
                        return reports;
                    }
                }
          
        }

        public void show_SuccessMessgae(string msg)
        {
            msg = msg.Replace("'", "|");
            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "Successmessage", "Successmessage('" + msg + "');", true);
        }
        public void show_ErrorMessgae(string msg)
        {
            msg = msg.Replace("'", "|");
            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "errormessage", "errormessage('" + msg + "');", true);
        }
    }


}