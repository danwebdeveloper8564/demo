﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="RominaWebApplication.LogIn" %>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8" />
    <title>LogIn</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/style.css" rel="stylesheet" />
    <!-- Bootstrap 3.3.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="css/style.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById('<%= txtUserID.ClientID%>').focus();
          });
    </script>
    <style type="text/css">
        element.style {
            margin-bottom:0;
        }
    </style>
</head>
<body style="margin: 0 !important; background-image: url(img/wmap.png);">
    <form runat="server" defaultfocus="txtUserID" style="border: none; padding: 0em 2em 2em 2em; margin-right: 11%;">
        <div>
            <table>
                <tr>
                    <td style="width: 80px; display: none;">
                        </td>
                    <td style="font-family: Arial; color: #66d3f5; font-size: x-large;"><b>LOGIN & GET STARTED</b></td>
                </tr>

            </table>
        </div>
        <div class="group">
            <div id="BootstrapErrorMessage" runat="server" class="alert alert-danger" visible="false">
                <asp:Label ID="errormsglabel" runat="server" Text="Label"></asp:Label>
            </div>
        </div>

        <div class="group">
            <asp:DropDownList ID="ddlFY" runat="server" Width="100%" Height="30px" Style="color: black; border: none" Visible="false"></asp:DropDownList>
        </div>

        <div class="group">
            <label for="exampleInputEmail1" style="color: #a3a2a2;">Email Address</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;"></label>
            <input type="text" id="txtUserID" runat="server" style="background-color: #fff; color: #a3a2a2; border: 1px solid #eaeaea; height: 32px;"><span class="highlight"></span><span class="bar"></span>
            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ErrorMessage="Please Enter Username" CssClass="alert-danger"
                Display="Static" SetFocusOnError="true" ForeColor="Red" ValidationGroup="login" ControlToValidate="txtUserID"></asp:RequiredFieldValidator>
        </div>

        <div class="group" style="margin-bottom: 10px;">
            <label for="exampleInputEmail1" style="color: #a3a2a2;">Password</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;"></label>

            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Style="background-color: #fff; height: 32px; color: #a3a2a2; border: 1px solid #eaeaea;"></asp:TextBox>
            <span class="highlight"></span><span class="bar"></span>
            <asp:RequiredFieldValidator ID="rfvPwd" runat="server" ErrorMessage="Please Enter Password" CssClass="alert-danger"
                Display="Static" SetFocusOnError="true" ForeColor="Red" ValidationGroup="login" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>

        </div>
        <div class="group" style="text-align: right; font-size: 1.2em;">
            <asp:LinkButton ID="LinkButton2" runat="server" ForeColor="#6b6b6b" Font-Underline="false" PostBackUrl="~/forgotpassword.aspx">Forgotten Password?</asp:LinkButton>
        </div>

        <div class="group" style="margin-bottom: 0;">
            <asp:Button ID="Button1" runat="server" Text="Login" class="button buttonBlue gradient-green" Style="width: 30% !important; background-color: #08b108;" ValidationGroup="login" OnClick="btnSubmit_Click" />
        </div>
    </form>
    <div class="pull-right">
    </div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="js/index.js"></script>
</body>


