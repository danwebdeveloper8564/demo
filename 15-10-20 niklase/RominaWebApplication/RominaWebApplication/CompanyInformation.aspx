﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FFMS.Master" AutoEventWireup="true" CodeBehind="CompanyInformation.aspx.cs" Inherits="RominaWebApplication.CompanyInformation" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <style type="text/css">

        .spinner {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -50px; /* half width of the spinner gif */
            margin-top: -50px; /* half height of the spinner gif */
            text-align: center;
            z-index: 999;
            overflow: auto;
            width: 100px; /* width of the spinner gif */
            height: 102px; /*hight of the spinner gif +2px to fix IE8 issue */
        }
    </style>
    <script type="text/javascript">
        var V1 = "";
        function errormessage(V1) {
            $("#messageNotification").jqxNotification({
                width: 300, position: "top-right", opacity: 2,
                autoOpen: false, animationOpenDelay: 800, autoClose: true, autoCloseDelay: 3000, template: "error"
            });
            $('#<%=lblmasg.ClientID %>').html(V1);
            $("#messageNotification").jqxNotification("open");

        }
    </script>
    <script type="text/javascript">
        var V = "";
        function Successmessage(V) {
            $("#messageNotification").jqxNotification({
                width: 300, position: "top-right", opacity: 2,
                autoOpen: false, animationOpenDelay: 800, autoClose: true, autoCloseDelay: 3000, template: "success"
            });
            $('#<%=lblmasg.ClientID %>').html(V);
            $("#messageNotification").jqxNotification("open");
        }
    </script>
    <script type="text/javascript">
        function validate() {
            if ($('#<%=txtCompanyName.ClientID%>').val() == "") {
                errormessage("Please enter Company Name.");
                return false;
            }

            var value = ($('#<%=txtCompanyName.ClientID%>').val().charAt(0));
            var chrcode = value.charCodeAt(0);
            if ((chrcode < 97 || chrcode > 122) && (chrcode < 65 || chrcode > 90)) {
                errormessage("Do not start Name with special characters.")
                return false;
            }
            if ($('#<%=txtAddress1.ClientID%>').val() == "") {
                errormessage("Please enter Address1.");
                return false;
            }
            if ($('#<%=txtCity.ClientID%>').val() == "") {
                errormessage("Please enter City.");
                return false;
            }
            if ($('#<%=txtState.ClientID%>').val() == "") {
                errormessage("Please enter State.");
                return false;
            }
            if ($('#<%=txtCountry.ClientID%>').val() == "") {
                errormessage("Please enter Country.");
                return false;
            }
            if ($('#<%=txtPhoneno.ClientID%>').val() == "") {
                errormessage("Please enter Phone No.");
                return false;
            }
            if ($('#<%=txtEmail.ClientID%>').val() == "") {
                errormessage("Please enter E-mail.");
                return false;
            }
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#<%=txtEmail.ClientID%>').val())) {
                return (true)
            }
            else {
                errormessage("You have entered an invalid email address!")
                return (false)
            }

            if ($('#<%=txtSMTP.ClientID%>').val() == "") {
                errormessage("Please enter Server.");
                return false;
            }
            if ($('#<%=txtPort.ClientID%>').val() == "") {
                errormessage("Please enter Port No.");
                return false;
            }
            if ($('#<%=txtSEmail.ClientID%>').val() == "") {
                errormessage("Please enter E-mail.");
                return false;
            }
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#<%=txtSEmail.ClientID%>').val())) {
                return (true)
            }
            else {
                errormessage("You have entered an invalid email address!")
                return (false)
            }
            if ($('#<%=txtPass.ClientID%>').val() == "") {
                errormessage("Please enter Password.");
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var valLength = "";
            $('#<%=txtCompanyName.ClientID%>').keypress(function (key) {

                valLength = ($('#<%=txtCompanyName.ClientID%>').val().length + 1);

                if (valLength < 2) {
                    if ((key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) && key.charCode != 32) return false;
                }
                else {
                    return true;
                }
            });
        });
    </script>
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are you sure to delete?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <script type="text/javascript">
        function DoNav(Id) {
            if (Id != "") {
                $('#spinner').show();
                __doPostBack('', Id)
            }
        }
    </script>
    <section class="content">
        <div id="spinner" class="spinner" style="display: none;">
            <img id="img-spinner" src="img/loader.gif" alt="Loading" /><br />
            Loading Data....
        </div>
        <div id="messageNotification">
            <div>
                <asp:Label ID="lblmasg" runat="server"></asp:Label>
            </div>
        </div>
        <div class="box-body" id="mainDiv" runat="server">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-default">
                        <div class="box-header">
                            <h3 class="box-title">Company Information</h3>
                             <asp:HiddenField  ID="hidCompanyID" runat="server"/>
                            <div style="float: right">
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                               
                            <div class="col-lg-5 col-md-6 col-sm-8 col-xs-10">
                                <div class="form-group">
                                    <input id="DepId" hidden="hidden" />
                                    <label for="exampleInputEmail1">Company Name:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control" maxlength="50" id="txtCompanyName" placeholder="Company Name">
                                </div>
                                 <div class="form-group">
                                        <label for="exampleInputEmail1">Address1:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="255"  CssClass="form-control" placeholder="Enter Address"  Style="resize: none;" TextMode="SingleLine"></asp:TextBox>
                                      </div>

                                   <div class="form-group">
                                        <label for="exampleInputEmail1">Address2:</label>
                                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="255"  CssClass="form-control" placeholder="Enter Address"  Style="resize: none;" TextMode="SingleLine"></asp:TextBox>
                                      </div>

                                 <div class="form-group">
                                  
                                    <label for="exampleInputEmail1">City:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control" maxlength="50" id="txtCity" placeholder="City Name">
                                </div>
                                 <div class="form-group">
                                   
                                    <label for="exampleInputEmail1">State:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control" maxlength="50" id="txtState" placeholder="State Name">
                                </div>
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Country:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control" maxlength="50" id="txtCountry" placeholder="Country Name">
                                </div>
                                  <div class="form-group">
                                   
                                    <label for="exampleInputEmail1">Phone No.:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control numeric" maxlength="15" id="txtPhoneno" placeholder="Phone No.">
                                </div>
                                  <div class="form-group">
                                   
                                    <label for="exampleInputEmail1">Fax No.:</label>
                                    <input runat="server" type="text" class="form-control numeric" maxlength="20" id="txtFaxno" placeholder="Fax No.">
                                </div>
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Email :</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control" maxlength="50" id="txtEmail" placeholder="Email">
                                </div>

                                             <div class="form-group">
                                    <label for="exampleInputEmail1">SMTP Server:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control" maxlength="50" id="txtSMTP" placeholder="Server">
                                </div>
                                  <div class="form-group">
                                   
                                    <label for="exampleInputEmail1">Port No.:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control numeric" maxlength="3" id="txtPort" placeholder="Port No.">
                                </div>
                                  <div class="form-group">
                                   
                                    <label for="exampleInputEmail1">Sending Email ID:</label>
                                    <input runat="server" type="text" class="form-control" maxlength="50" id="txtSEmail" placeholder="Email Id">
                                </div>
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Password :</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;">*</label>
                                    <input runat="server" type="text" class="form-control" maxlength="20" id="txtPass" placeholder="Password">
                                </div>

                                <div id="powerBIDIV" runat="server">
                                     <div class="form-group formlay">
                                         <label for="exampleInputEmail1">Power BI UserName(Id) :</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;"></label>
                                         <input runat="server" type="text" class="form-control" maxlength="100" id="txtPBIUserName" placeholder="Power BI UserName">
                                     </div>
                                     <div class="form-group formlay">
                                         <label for="exampleInputEmail1">Power Bi Password:</label>&nbsp;&nbsp;<label for="requiredFields" style="color: red;"></label>
                                         <input runat="server" type="text" class="form-control" maxlength="50" id="txtPBIPassword" placeholder="Power BI Password">
                                     </div>
                                 </div>

                            </div>
                               
                                   
                                    
                            </div>
                        </div>
                        <div class="box-footer">
                            <asp:Button Style="margin-right: 5px;" type="button" ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" OnClientClick="javascript:return validate();" />
                            <asp:Button Style="margin-right: 5px;" type="button" ID="btnCancel" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btnCancel_Click" />
                            <asp:Button Style="margin-right: 5px;" type="button" ID="btnDelete" runat="server" Text="Delete" class="btn btn-primary" OnClientClick="Confirm()" OnClick="btnDelete_Click" />
                        </div>
                          <br />
                           <div>
                            <b> </b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable({ "order": [[0, "desc" ]] } );
        });
    </script>
    <script src="Scripts/jquery.numeric.min.js"></script>
        <script type="text/javascript">
            $(".numeric").numeric();
    </script>

</asp:Content>
