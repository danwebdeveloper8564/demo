﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using DAL;

namespace BusinessLayer
{
    #region

    public class Settings
    {
        public Settings() { }
        ~Settings() { }
        static Settings objSettings;
        static readonly object padlock = new object();

        /// <summary>
        /// Returns instance of class object
        /// </summary>
        public static Settings Instance
        {
            get
            {
                return GetInstance();
            }
        }
        private static Settings GetInstance()
        {
            lock (padlock)
            {
                //if (objSettings == null)
                objSettings = new Settings();
            }

            return objSettings;
        }
       

        public String Role
        {
            get { return (HttpContext.Current.Session["Role"] != null ? Convert.ToString(HttpContext.Current.Session["Role"]) : "0"); }
            set { HttpContext.Current.Session["Role"] = value; }
        }
        //Ankita - 03/may/2016- (For Optimization)
        public String RoleType
        {
            get { return (HttpContext.Current.Session["RoleType"] != null ? Convert.ToString(HttpContext.Current.Session["RoleType"]) : "0"); }
            set { HttpContext.Current.Session["RoleType"] = value; }
        }
        public String EmpName
        {
            get { return (HttpContext.Current.Session["EmpName"] != null ? Convert.ToString(HttpContext.Current.Session["EmpName"]) : "0"); }
            set { HttpContext.Current.Session["EmpName"] = value; }
        }
        public String UserID
        {
            get { return (HttpContext.Current.Session["UserID"] != null ? Convert.ToString(HttpContext.Current.Session["UserID"]) : "0"); }
            set { HttpContext.Current.Session["UserID"] = value; }
        }
       
        public String RoleID
        {
            get { return (HttpContext.Current.Session["RoleID"] != null ? Convert.ToString(HttpContext.Current.Session["RoleID"]) : "0"); }
            set { HttpContext.Current.Session["RoleID"] = value; }
        }
       
        public String UserName
        {
            get { return Convert.ToString(HttpContext.Current.Session["UserName"]); }
            set { HttpContext.Current.Session["UserName"] = value; }
        }
        
        
        public String UserIP
        {
            get { return IPAddress(); }
        }
        private string IPAddress()
        {
            string strIpAddress;
            strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (strIpAddress == null)
                strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            return strIpAddress;
        }
        public void KillSession(string Url, Page page)
        {
            string script = "windows.history.clear(); window.onload = 'history.go(+1)';";
            ScriptManager.RegisterClientScriptBlock(page, GetType(), "HistoryKey", script, true);
            KillSession("Loged Out");
            RedirectTopPage(Url, page);
        }
        public void KillSession(string Remarks)
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetAllowResponseInBrowserHistory(false);
            HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.ToUniversalTime().AddSeconds(19800).AddSeconds(-1));
            HttpContext.Current.Response.Cache.SetNoStore();
            HttpContext.Current.Response.AppendHeader("Pragma", "no-cache");
            objSettings = null;
        }

        public void RedirectCurrentPage(string Url, Page page)
        {
            Control ctl = new Control();
            string script = "window.location = '" + ctl.ResolveUrl(Url) + "';";
            //string script = "top.window.location = '" + Url + "';";
            ScriptManager.RegisterClientScriptBlock(page, GetType(), "RedirectCurrKey", script, true);
        }
        public void RedirectTopPage(string Url, Page page)
        {
            Control ctl = new Control();
            string script = "top.window.location = '" + ctl.ResolveUrl("~/" + Url) + "';";
            //string script = "top.window.location = '" + Url + "';";
            ScriptManager.RegisterClientScriptBlock(page, GetType(), "RedirectTopKey", script, true);
        }
        public string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name.ToLower();
            return sRet;
        }
        public string GetCurrentPageNameWithQueryString()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            string sQuery = System.Web.HttpContext.Current.Request.Url.Query;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet + sQuery;
        }

        public String FY
        {
            get { return (HttpContext.Current.Session["FY"] != null ? Convert.ToString(HttpContext.Current.Session["FY"]) : "0"); }
            set { HttpContext.Current.Session["FY"] = value; }
        }

        public static string GetDocID(string Name, DateTime da)
        {
            DbParameter[] dbParam = new DbParameter[3];
            dbParam[0] = new DbParameter("@V_Type", DbParameter.DbType.VarChar, 5, Name);
            dbParam[1] = new DbParameter("@V_Date", DbParameter.DbType.DateTime, 8, da);
            dbParam[2] = new DbParameter("@mDocId", DbParameter.DbType.VarChar, 35, ParameterDirection.Output);
            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Getdocid", dbParam);
            return Convert.ToString(dbParam[2].Value);
        }

        public static void SetDocID(string Name, string docID)
        {
            DbParameter[] dbParam = new DbParameter[2];
            dbParam[0] = new DbParameter("@V_Type", DbParameter.DbType.VarChar, 5, Name);
            dbParam[1] = new DbParameter("@mDocId", DbParameter.DbType.VarChar, 35, docID);
            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Setdocid", dbParam);
        }
        public static DateTime GetUTCTime()
        {
            DateTime dt = DateTime.UtcNow;
            DateTime newDate = dt.AddHours(+5.30);
            return newDate;
        }
        public static string dateformat(string da)
        {
            DateTime todt = DateTime.Parse(da);
            string d1 = todt.ToString("MM/dd/yyyy");
            return d1;
        }
        public static string dateformat1(string da)
        {
            DateTime todt = DateTime.Parse(da);
            string d1 = todt.ToString("MM/dd/yyyy");
            return d1;
        }
        public static int UnderUsersforlowest(string LoginSMID)
        {
            string str1 = @"select min(lvl) from MastSalesRep left join MastRole on MastRole.RoleId=MastSalesRep.RoleId
            where smid in (select distinct smid from MastSalesRepGrp where smid in (select smid from MastSalesRepGrp where MainGrp in (" + LoginSMID + ")) and  level>= (select distinct level from MastSalesRepGrp where MainGrp in (" + LoginSMID + " ))) and MastSalesRep.Active=1";
            int level = Convert.ToInt16(DAL.DbConnectionDAL.GetScalarValue(CommandType.Text, str1));
            return level;
        }
        public static DataTable UnderUsers(string LoginSMID)
        {
            string str1 = @"select mastrole.RoleName,MastSalesRep.*,mastrole.RoleType from MastSalesRep left join MastRole on MastRole.RoleId=MastSalesRep.RoleId
            where smid in (select distinct smid from MastSalesRepGrp where smid in (select smid from MastSalesRepGrp where MainGrp in (" + LoginSMID + ")) and  level>= (select distinct level from MastSalesRepGrp where MainGrp in (" + LoginSMID + " ))) and MastSalesRep.Active=1 order by MastSalesRep.SMName";
            DataTable dt1 = new DataTable();
            dt1 = DbConnectionDAL.GetDataTable(CommandType.Text, str1);
            return dt1;
        }
        public static DataTable FindunderUsers(string LoginSMID)
        {

            string str1 = @"select mastrole.RoleName,MastSalesRep.*,mastrole.RoleType from MastSalesRep left join MastRole on MastRole.RoleId=MastSalesRep.RoleId
            where smid in (select distinct smid from MastSalesRepGrp where smid in (select smid from MastSalesRepGrp where MainGrp in (" + LoginSMID + "))) and MastSalesRep.Active=1 order by MastSalesRep.SMName";
            DataTable dt1 = new DataTable();
            dt1 = DbConnectionDAL.GetDataTable(CommandType.Text, str1);
            return dt1;
        }

        public static DataTable BindUnderUsersApproval(string LoginSMID)
        {

            string str1 = @"select mastrole.RoleName,MastSalesRep.*,mastrole.RoleType from MastSalesRep left join MastRole on MastRole.RoleId=MastSalesRep.RoleId
            where smid in (select distinct smid from MastSalesRepGrp where smid in (select smid from MastSalesRepGrp where MainGrp in (" + LoginSMID + ")) and  level> (select distinct level from MastSalesRepGrp where MainGrp in (" + LoginSMID + " ))) and MastSalesRep.Active=1 order by MastSalesRep.SMName";
            DataTable dt1 = new DataTable();
            dt1 = DbConnectionDAL.GetDataTable(CommandType.Text, str1);
            return dt1;
        }

        public string CheckPagePermissions(string pageName, string UID)
        {
            string allPerm = string.Empty;
            if (!string.IsNullOrEmpty(UID))
            {
                int PageID = PageIDfromPageName(pageName);

                string PagePermission = @"select  case ViewP when 1 then 'true' else 'false' end +','+ case AddP when 1 then 'true' else 'false' end 
+','+ case EditP when 1 then 'true' else 'false' end +','+ case DeleteP when 1 then 'true' else 'false' end +','
+ case ExportP when 1 then 'true' else 'false' end  as Permission from MastRolePermission where PageId=" + PageID + " and RoleId=" + Settings.Instance.RoleID + "";
                object onj = DbConnectionDAL.GetScalarValue(CommandType.Text, PagePermission);
                if (onj != null)
                {
                    allPerm = onj.ToString();
                }
                else
                {
                    allPerm = "false,false,false,false,false";
                }
            }
            else
            {
                allPerm = "false,false,false,false,false";
            }
            return allPerm;
        }
        public int RollID(string UID)
        {
            int rolID = 0;
            string envObj = @"select * from MastLogin where Name='" + Convert.ToString(UID) + "'";
            DataTable RollDT = DbConnectionDAL.GetDataTable(CommandType.Text, envObj);
            if (RollDT.Rows.Count > 0)
            {
                rolID = Convert.ToInt32(RollDT.Rows[0]["RoleId"].ToString());
            }
            return rolID;
        }
        public bool CheckViewPermission(string pageName, string UID)
        {
            Boolean edit = false;
            int PageID = PageIDfromPageName(pageName);
            int rollID = RollID(UID);
            string rolePermission = @"select ViewP from MastRolePermission where PageId=" + PageID + " and RoleId=" + rollID + "";
            DataTable rolePDT = DbConnectionDAL.GetDataTable(CommandType.Text, rolePermission);
            if (rolePDT.Rows.Count > 0)
            {
                edit = Convert.ToBoolean(rolePDT.Rows[0]["ViewP"]);

            }
            return edit;
        }
        public bool CheckAddPermission(string pageName, string UID)
        { 
            Boolean add = false;
            int PageID = PageIDfromPageName(pageName);
            int rollID = RollID(UID);
            string rolePermission = @"select AddP from MastRolePermission where PageId=" + PageID + " and RoleId=" + rollID + "";
            DataTable rolePDT = DbConnectionDAL.GetDataTable(CommandType.Text, rolePermission);
            if (rolePDT.Rows.Count > 0)
            {
                add = Convert.ToBoolean(rolePDT.Rows[0]["AddP"]);

            }
            return add;
        }
        public bool CheckEditPermission(string pageName, string UID)
        {
            Boolean edit = false;
            int PageID = PageIDfromPageName(pageName);
            int rollID = RollID(UID);
            string rolePermission = @"select EditP from MastRolePermission where PageId=" + PageID + " and RoleId=" + rollID + "";
            DataTable rolePDT = DbConnectionDAL.GetDataTable(CommandType.Text, rolePermission);
            if (rolePDT.Rows.Count > 0)
            {
                edit = Convert.ToBoolean(rolePDT.Rows[0]["EditP"]);

            }
            return edit;
        }
        public bool CheckDeletePermission(string pageName, string UID)
        {
            Boolean delete = false;
            int PageID = PageIDfromPageName(pageName);
            int rollID = RollID(UID);
            string rolePermission = @"select DeleteP from MastRolePermission where PageId=" + PageID + " and RoleId=" + rollID + "";
            DataTable rolePDT = DbConnectionDAL.GetDataTable(CommandType.Text, rolePermission);
            if (rolePDT.Rows.Count > 0)
            {
                delete = Convert.ToBoolean(rolePDT.Rows[0]["DeleteP"]);
            }
            return delete;

        }

        public int PageIDfromPageName(string PageName)
        {
            string getPageIdQry = @"select PageId from MastPage where PageName='" + PageName + "' ";
            DataTable getPageDT = DbConnectionDAL.GetDataTable(CommandType.Text, getPageIdQry);
            //return (from h in op.MastPages.Where(u => u.PageName == PageName)
            //        select h.PageId).SingleOrDefault();
            if (getPageDT.Rows.Count > 0)
            {
                return Convert.ToInt32(getPageDT.Rows[0]["PageId"]);
            }
            else
            {
                return 0;
            }
        }


        public static Int32 DMInt32(string val)
        {
            Int32 a = 0;
            try { a = Convert.ToInt32(val); }
            catch { }
            return a;
        }
        public static Int64 DMInt64(string val)
        {
            Int64 a = 0;
            try { a = Convert.ToInt64(val); }
            catch { }
            return a;
        }
        public static int DMInt(string val)
        {
            int a = 0;
            try { a = Convert.ToInt32(val); }
            catch { }
            return a;
        }
        public static decimal DMDecimal(string val)
        {
            decimal a = 0;
            try { a = Convert.ToDecimal(val); }
            catch { }
            return a;
        }

        public int CreateTable(string createQuery)
        {
            return DbConnectionDAL.ExecuteQuery(createQuery);
        }
        public bool isAdmin(string userId)
        {
            string qry = @"select count(*) from MastLogin where Id='"+userId+"' and RoleId=(select RoleId from MastRole where isAdmin=1)";
            int count = Convert.ToInt32(DbConnectionDAL.GetScalarValue(CommandType.Text, qry));
            if (count > 0)
                return true;
            else
                return false;
        }
        
        public String PBI_UserName
        {
            get { return (HttpContext.Current.Session["PBI_UserName"] != null ? Convert.ToString(HttpContext.Current.Session["PBI_UserName"]) : ""); }
            set { HttpContext.Current.Session["PBI_UserName"] = value; }
        }
        public String PBI_Password
        {
            get { return (HttpContext.Current.Session["PBI_Password"] != null ? Convert.ToString(HttpContext.Current.Session["PBI_Password"]) : ""); }
            set { HttpContext.Current.Session["PBI_Password"] = value; }
        }


    }
    #endregion
}
