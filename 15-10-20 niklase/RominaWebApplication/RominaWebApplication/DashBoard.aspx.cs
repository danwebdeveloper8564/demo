﻿using System;
using System.Web.UI.WebControls;


namespace RominaWebApplication
{
    public partial class DashBoard : System.Web.UI.Page
    {
        string str = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    tab_bi.Visible = true;
                    tab_chart.Visible = false;
                    Populate_ReportsDropdown_and_LoadFirst();
                }
            }
            catch (Exception ex)
            {
                show_ErrorMessgae("Server Error : " + ex.Message);
            }
        }


        private void Populate_ReportsDropdown_and_LoadFirst()
        {
            if (Session["AccessToken"] != null)
            {
                accessToken.Value = Session["AccessToken"].ToString();
                Populate_Dropdown_WithDashboards();
                if (ddlDashboards.Items.Count > 1)
                {
                    ddlDashboards.SelectedIndex = 1;
                    ddlDashboards_SelectedIndexChanged(new object(), new EventArgs());
                }
            }
        }

        private void Populate_Dropdown_WithDashboards()
        {
            PBIDashboards Reports = Session["Dashboards_Object_List"] as PBIDashboards;

            ddlDashboards.Items.Clear();
            ddlDashboards.Items.Add(new ListItem("--Select Report--", ""));
            if (Reports.value.Length > 0)
            {
                for (int i = 0; i < Reports.value.Length; i++)
                {
                    var report1 = Reports.value[i];
                    ddlDashboards.Items.Add(new ListItem(report1.displayName, report1.displayName));
                }
            }
        }
        private void Set_Dashboard(int ofIndex)
        {
            PBIDashboards Dashboard = Session["Dashboards_Object_List"] as PBIDashboards;
            
            var dashboard = Dashboard.value[ofIndex];

            hdn_EmbedURL.Value = dashboard.embedUrl;
            hdn_reportID.Value = dashboard.id;
            hdn_reportName.Value = dashboard.displayName;
        }
        
        protected void ddlDashboards_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDashboards.SelectedIndex > 0)
            {
                Set_Dashboard(ddlDashboards.SelectedIndex - 1);
            }
            else
            {
                hdn_EmbedURL.Value = "";
                hdn_reportID.Value = "";
                hdn_reportName.Value = "";
            }
        }
        public void show_SuccessMessgae(string msg)
        {
            msg = msg.Replace("'", "|");
            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "Successmessage", "Successmessage('" + msg + "');", true);
        }
        public void show_ErrorMessgae(string msg)
        {
            msg = msg.Replace("'", "|");
            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "errormessage", "errormessage('" + msg + "');", true);
        }

    }
}
