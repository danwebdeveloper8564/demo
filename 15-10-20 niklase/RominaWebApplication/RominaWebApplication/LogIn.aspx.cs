﻿using BusinessLayer;
using DAL;
using System;
using System.Data;
using System.Web.UI;

namespace RominaWebApplication
{
    public partial class LogIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Settings.Instance.UserID = "0";
                FillFY();
            }
        }
        private void FillFY()
        {
            string str = "select * from Financial_Year order by Financial_Year desc";
            DataTable dt = DbConnectionDAL.GetDataTable(CommandType.Text,str);
            if(dt.Rows.Count>0)
            {
                ddlFY.DataSource = dt;
                ddlFY.DataTextField = "Financial_Year";
                ddlFY.DataValueField = "Financial_Year";
                ddlFY.DataBind();
            }
            ddlFY.SelectedValue =Convert.ToString(DateTime.Now.Year);
        }
        private bool setPowerBICredentials_In_Session(string uname,string pwd)
        {
            if (uname == "" || pwd == "")
                return false;

            Settings.Instance.PBI_UserName = uname;
            Settings.Instance.PBI_Password = pwd;

            return true;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
           
            if (Page.IsValid)
            {
                if (IsvalidUser(txtUserID.Value, txtPassword.Text))
                {

                    Session["user_name"] = txtUserID.Value;
                    if (Settings.Instance.RoleID == "18")
                    {
                        Session["ROle"] = "Admin";
                        
                        string qry = @"select * from Company_Information";
                        DataTable dt = DbConnectionDAL.GetDataTable(CommandType.Text, qry);

                        setPowerBICredentials_In_Session(dt.Rows[0]["Bi_User_Name"].ToString(), dt.Rows[0]["Bi_Password"].ToString());
                        Response.Redirect("/PowerBI_Controller.aspx?interfaceID=Report", true);

                    }
                   
                    else
                    { }
                }
                else
                {
                    BootstrapErrorMessage.Visible = true;
                    errormsglabel.Text = "Invalid Username or Password";
                }
            }
            else
            {}
        }

        private void Make_Entry_InLog(string MastLogin__Name)
        {
            try
            {
                string qry = @"select * from Store where Email= (select Email from MastLogin where Name=@Name)";
                DbParameter[] dbParam = new DbParameter[1];
                dbParam[0] = new DbParameter("@Name", DbParameter.DbType.VarChar, 50, MastLogin__Name);

                DataTable dt = DbConnectionDAL.GetDataTable(CommandType.Text, qry, dbParam);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];

                    string qry_Insert = @"
            INSERT INTO [dbo].[Login_Log]
                        ([Store_Code]
                        ,[Store_Name]
                        ,[Email]
                        ,[Timestamp])
                    VALUES
                        ('" + dr["Store_Code"].ToString() + "','" + dr["Agent_Name"].ToString() + "','" + dr["Email"].ToString() + "',GETDATE())";

                    DbConnectionDAL.ExecuteNonQuery(CommandType.Text, qry_Insert);
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        private bool IsvalidUser(string userName, string password)
        {
            bool IsFlag = false;
            
            DbParameter[] dbParam = new DbParameter[2];
            dbParam[0] = new DbParameter("@userID", DbParameter.DbType.VarChar, 100, userName);
            dbParam[1] = new DbParameter("@pwd", DbParameter.DbType.VarChar, 20, password);

            DataTable logindt = DbConnectionDAL.GetDataTable(CommandType.StoredProcedure, "sp_ChkLogin", dbParam);
            if (logindt.Rows.Count > 0)
            {
                if (!Convert.ToBoolean(logindt.Rows[0]["Active"]))
                {
                    errormsglabel.Text = "This User has been disabled.Please contact your administrator.";
                    IsFlag = false;
                }
                else
                {
                    if (Settings.Instance.UserID.ToString() != "0")
                    {
                        if (Settings.Instance.UserID.ToString() != Convert.ToString(logindt.Rows[0]["Id"]))
                        {
                            IsFlag = false;
                            errormsglabel.Text = "Another or Same User Already logged in Same Browser....!";
                            
                        }
                        else
                        {
                            IsFlag = false;                           
                            errormsglabel.Text = "Same User Already logged in Same Browser....!";
                        }
                    }

                    else
                    {
                        Settings.Instance.FY = ddlFY.SelectedValue;
                        Settings.Instance.UserID = Convert.ToString(logindt.Rows[0]["Id"]);
                        Settings.Instance.UserName = Convert.ToString(logindt.Rows[0]["Name"]);
                        Settings.Instance.RoleID = Convert.ToString(logindt.Rows[0]["RoleId"]);
                        Settings.Instance.RoleType = Convert.ToString(logindt.Rows[0]["RoleType"]);
                        Settings.Instance.EmpName = Convert.ToString(logindt.Rows[0]["empname"]);
                        IsFlag = true;
                    }
                }
            }
            else
            {
                errormsglabel.Text = "Invalid User Name or Password.";
                IsFlag = false;
            }
            return IsFlag;
        }
    }
}