﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.FinancialYear
{
    public class FinancialYear
    {
        public int Insert(int ID, string FinancialYear, string Start_Date, string End_Date,bool ActiveStatus)
        {
            DbParameter[] dbParam = new DbParameter[7];

            DateTime startDt = Convert.ToDateTime(Start_Date);
            DateTime endDt = Convert.ToDateTime(End_Date);
            //startDt = startDt.Date;
            //endDt = endDt.Date;

            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, 0);
            
            dbParam[1] = new DbParameter("@Financial_Year", DbParameter.DbType.Int, 4, FinancialYear);
            dbParam[2] = new DbParameter("@Start_Date", DbParameter.DbType.DateTime, 50, startDt);
            dbParam[3] = new DbParameter("@End_Date", DbParameter.DbType.DateTime, 50, endDt);
            dbParam[4] = new DbParameter("@Active", DbParameter.DbType.TinyInt, 1, ActiveStatus);

            dbParam[5] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Insert");
            dbParam[6] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);

            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Financial_Year_ups", dbParam);
            return Convert.ToInt32(dbParam[6].Value);
        }

        public int Update(int ID, string FinancialYear, string Start_Date, string End_Date, bool ActiveStatus)
        {
            DbParameter[] dbParam = new DbParameter[7];

            DateTime startDt = Convert.ToDateTime(Start_Date);
            
            DateTime endDt = Convert.ToDateTime(End_Date);


            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, ID);

            dbParam[1] = new DbParameter("@Financial_Year", DbParameter.DbType.Int, 4, FinancialYear);
            
            dbParam[2] = new DbParameter("@Start_Date", DbParameter.DbType.DateTime, 50, startDt);
            dbParam[3] = new DbParameter("@End_Date", DbParameter.DbType.DateTime, 50, endDt);

            dbParam[4] = new DbParameter("@Active", DbParameter.DbType.TinyInt, 1, ActiveStatus);

            dbParam[5] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Update");
            dbParam[6] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);

            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Financial_Year_ups", dbParam);
            return Convert.ToInt32(dbParam[6].Value);
        }

        public int delete(string Financial_YearId)
        {
            DbParameter[] dbParam = new DbParameter[2];

            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, Convert.ToInt32(Financial_YearId));

            dbParam[1] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Financial_Year_del", dbParam);

            return Convert.ToInt32(dbParam[1].Value);
        }
    }
}
