﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Store_Template
{
    public class StoreTemplateHeader
    {
        public int Insert(int ID, string storeId,string storeName,string Template_Name,string Template_Type,string User)
        {
            DbParameter[] dbParam = new DbParameter[8];

            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, 0);
            
            dbParam[1] = new DbParameter("@StoreId", DbParameter.DbType.Int, 50, storeId);
            dbParam[2] = new DbParameter("@Store_Name", DbParameter.DbType.VarChar, 50, storeName);

            dbParam[3] = new DbParameter("@Template_Name", DbParameter.DbType.VarChar, 50, Template_Name);
            dbParam[4] = new DbParameter("@Template_Type", DbParameter.DbType.VarChar, 50, Template_Type);
            
            dbParam[5] = new DbParameter("@User", DbParameter.DbType.VarChar, 50, User);

            dbParam[6] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Insert");
            dbParam[7] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);

            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Store_Template_Header_ups", dbParam);
            return Convert.ToInt32(dbParam[7].Value);
        }

        public int Update(int ID, string Code, string Name)
        {
            DbParameter[] dbParam = new DbParameter[5];
            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, ID);
            dbParam[1] = new DbParameter("@Code", DbParameter.DbType.VarChar, 20, Code);
            dbParam[2] = new DbParameter("@Name", DbParameter.DbType.VarChar, 50, Name);
            dbParam[3] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Update");
            dbParam[4] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Country_ups", dbParam);
            return Convert.ToInt32(dbParam[4].Value);
        }

        public int delete(string CurrencyId)
        {
            DbParameter[] dbParam = new DbParameter[2];
            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, Convert.ToInt32(CurrencyId));
            dbParam[1] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Country_del", dbParam);
            return Convert.ToInt32(dbParam[1].Value);
        }
    }
    public class StoreTemplateLine
    {
        public int Insert(int ID, string storeId, string storeName, string Template_Name, string Template_Type, string Field_Name)
        {
            DbParameter[] dbParam = new DbParameter[8];

            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, 0);

            dbParam[1] = new DbParameter("@StoreId", DbParameter.DbType.Int, 50, storeId);
            dbParam[2] = new DbParameter("@Store_Name", DbParameter.DbType.VarChar, 50, storeName);

            dbParam[3] = new DbParameter("@Template_Name", DbParameter.DbType.VarChar, 50, Template_Name);
            dbParam[4] = new DbParameter("@Template_Type", DbParameter.DbType.VarChar, 50, Template_Type);

            dbParam[5] = new DbParameter("@Field_Name", DbParameter.DbType.VarChar, 100, Field_Name);
            
            dbParam[6] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Insert");
            dbParam[7] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);

            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Store_Template_Line_ups", dbParam);
            return Convert.ToInt32(dbParam[7].Value);
        }

        //public int Update(int ID, string Code, string Name)
        //{
        //    DbParameter[] dbParam = new DbParameter[5];
        //    dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, ID);
        //    dbParam[1] = new DbParameter("@Code", DbParameter.DbType.VarChar, 20, Code);
        //    dbParam[2] = new DbParameter("@Name", DbParameter.DbType.VarChar, 50, Name);
        //    dbParam[3] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Update");
        //    dbParam[4] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
        //    DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Country_ups", dbParam);
        //    return Convert.ToInt32(dbParam[4].Value);
        //}

        //public int delete(string Table_Name)
        //{
        //    DbParameter[] dbParam = new DbParameter[2];
        //    dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, Convert.ToInt32(CurrencyId));
        //    dbParam[1] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
        //    DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Country_del", dbParam);
        //    return Convert.ToInt32(dbParam[1].Value);
        //}
    }
}

