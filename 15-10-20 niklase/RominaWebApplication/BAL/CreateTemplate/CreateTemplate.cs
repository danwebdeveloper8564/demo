﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.CreateTemplate
{
    public class CreateTemplateLine
    {
        public int Insert(int ID, string Template_Name,string Table_Name, string Template_Type,string Field_Name,string Field_Type,string Field_Size,string FY,string User)
        {
            DbParameter[] dbParam = new DbParameter[11];

            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, 0);
            
            dbParam[1] = new DbParameter("@Tb_Name", DbParameter.DbType.VarChar, 50, Table_Name);
            dbParam[2] = new DbParameter("@Template_Type", DbParameter.DbType.VarChar, 50, Template_Type);

            dbParam[3] = new DbParameter("@Field_Name", DbParameter.DbType.VarChar, 100, Field_Name);
            dbParam[4] = new DbParameter("@Field_Type", DbParameter.DbType.VarChar, 50, Field_Type);
            dbParam[5] = new DbParameter("@Field_Size", DbParameter.DbType.VarChar, 50, Field_Size);

            dbParam[6] = new DbParameter("@FY", DbParameter.DbType.VarChar, 50, FY);
            dbParam[7] = new DbParameter("@User", DbParameter.DbType.VarChar, 50, User);

            dbParam[8] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Insert");
            dbParam[9] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);

            dbParam[10] = new DbParameter("@Template_Name", DbParameter.DbType.VarChar, 50, Template_Name);

            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Base_Template_Line_ups", dbParam);
            return Convert.ToInt32(dbParam[9].Value);
        }

        public int Update(int ID, string Code, string Name)
        {
            DbParameter[] dbParam = new DbParameter[5];
            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, ID);
            dbParam[1] = new DbParameter("@Code", DbParameter.DbType.VarChar, 20, Code);
            dbParam[2] = new DbParameter("@Name", DbParameter.DbType.VarChar, 50, Name);
            dbParam[3] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Update");
            dbParam[4] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Country_ups", dbParam);
            return Convert.ToInt32(dbParam[4].Value);
        }

        public int delete(string CurrencyId)
        {
            DbParameter[] dbParam = new DbParameter[2];
            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, Convert.ToInt32(CurrencyId));
            dbParam[1] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Country_del", dbParam);
            return Convert.ToInt32(dbParam[1].Value);
        }
    }
    public class CreateTemplateHeader
    {
        public int Insert(int ID, string Template_Name, string Table_Name, string Template_Type)
        {
            DbParameter[] dbParam = new DbParameter[6];

            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, 0);

            dbParam[1] = new DbParameter("@Template_Name", DbParameter.DbType.VarChar, 50, Template_Name);
            dbParam[2] = new DbParameter("@Template_Type", DbParameter.DbType.VarChar, 50, Template_Type);
            dbParam[3] = new DbParameter("@Tb_Name", DbParameter.DbType.VarChar, 50, Table_Name);
            
            dbParam[4] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Insert");
            dbParam[5] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);

            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Base_Template_Header_ups", dbParam);
            return Convert.ToInt32(dbParam[5].Value);
        }

        //public int Update(int ID, string Code, string Name)
        //{
        //    DbParameter[] dbParam = new DbParameter[5];
        //    dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, ID);
        //    dbParam[1] = new DbParameter("@Code", DbParameter.DbType.VarChar, 20, Code);
        //    dbParam[2] = new DbParameter("@Name", DbParameter.DbType.VarChar, 50, Name);
        //    dbParam[3] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Update");
        //    dbParam[4] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
        //    DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Country_ups", dbParam);
        //    return Convert.ToInt32(dbParam[4].Value);
        //}

        //public int delete(string Table_Name)
        //{
        //    DbParameter[] dbParam = new DbParameter[2];
        //    dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, Convert.ToInt32(CurrencyId));
        //    dbParam[1] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
        //    DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Country_del", dbParam);
        //    return Convert.ToInt32(dbParam[1].Value);
        //}
    }
}

