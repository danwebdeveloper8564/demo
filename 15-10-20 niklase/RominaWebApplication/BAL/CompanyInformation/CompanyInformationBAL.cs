﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.CompanyInformation
{
    public class CompanyInformationBAL
    {
        public int Insert(int ID ,string  Company_Name,string  Address1,string Address2,string City,string State,string Country,string Phone_No,string Fax_No,string Email,
            string smtp, int port, string SMail, string pass, string PBIUsername, string PBIPassword)
        {
            DbParameter[] dbParam = new DbParameter[18];
            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, 0);
            dbParam[1] = new DbParameter("@Company_Name", DbParameter.DbType.VarChar, 50, Company_Name);
            dbParam[2] = new DbParameter("@Address1", DbParameter.DbType.VarChar,255, Address1);
            dbParam[3] = new DbParameter("@Address2", DbParameter.DbType.VarChar,255, Address2);
            dbParam[4] = new DbParameter("@City", DbParameter.DbType.VarChar,50, City);
            dbParam[5] = new DbParameter("@State", DbParameter.DbType.VarChar, 50, State);
            dbParam[6] = new DbParameter("@Country", DbParameter.DbType.VarChar, 50, Country);
            dbParam[7] = new DbParameter("@Phone_No", DbParameter.DbType.VarChar, 50, Phone_No);
            dbParam[8] = new DbParameter("@Fax_No", DbParameter.DbType.VarChar, 50, Fax_No);
            dbParam[9] = new DbParameter("@Email", DbParameter.DbType.VarChar, 50, Email);
            dbParam[10] = new DbParameter("@smtp", DbParameter.DbType.VarChar, 50, smtp);
            dbParam[11] = new DbParameter("@port", DbParameter.DbType.Int, 3, port);
            dbParam[12] = new DbParameter("@SMail", DbParameter.DbType.VarChar, 50, SMail);
            dbParam[13] = new DbParameter("@pass", DbParameter.DbType.VarChar, 50, pass);
            dbParam[14] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Insert");

            dbParam[15] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);

            dbParam[16] = new DbParameter("@BI_User_Name", DbParameter.DbType.VarChar, 100, PBIUsername);
            dbParam[17] = new DbParameter("@BI_Password", DbParameter.DbType.VarChar, 50, PBIPassword);


            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Company_Information_ups", dbParam);
            return Convert.ToInt32(dbParam[15].Value);
        }

        public int Update(int ID, string Company_Name, string Address1, string Address2, string City, string State, string Country, string Phone_No, string Fax_No, string Email,
             string smtp, int port, string SMail, string pass, string PBIUsername, string PBIPassword)
        {
            DbParameter[] dbParam = new DbParameter[18];
            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 10, ID);
            dbParam[1] = new DbParameter("@Company_Name", DbParameter.DbType.VarChar, 50, Company_Name);
            dbParam[2] = new DbParameter("@Address1", DbParameter.DbType.VarChar, 255, Address1);
            dbParam[3] = new DbParameter("@Address2", DbParameter.DbType.VarChar, 255, Address2);
            dbParam[4] = new DbParameter("@City", DbParameter.DbType.VarChar, 50, City);
            dbParam[5] = new DbParameter("@State", DbParameter.DbType.VarChar, 50, State);
            dbParam[6] = new DbParameter("@Country", DbParameter.DbType.VarChar, 50, Country);
            dbParam[7] = new DbParameter("@Phone_No", DbParameter.DbType.VarChar, 50, Phone_No);
            dbParam[8] = new DbParameter("@Fax_No", DbParameter.DbType.VarChar, 50, Fax_No);
            dbParam[9] = new DbParameter("@Email", DbParameter.DbType.VarChar, 50, Email);
            dbParam[10] = new DbParameter("@smtp", DbParameter.DbType.VarChar, 50, smtp);
            dbParam[11] = new DbParameter("@port", DbParameter.DbType.Int, 3, port);
            dbParam[12] = new DbParameter("@SMail", DbParameter.DbType.VarChar, 50, SMail);
            dbParam[13] = new DbParameter("@pass", DbParameter.DbType.VarChar, 50, pass);
            dbParam[14] = new DbParameter("@status", DbParameter.DbType.VarChar, 15, "Update");

            dbParam[15] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);

            dbParam[16] = new DbParameter("@BI_User_Name", DbParameter.DbType.VarChar, 100, PBIUsername);
            dbParam[17] = new DbParameter("@BI_Password", DbParameter.DbType.VarChar, 50, PBIPassword);

            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Company_Information_ups", dbParam);
            return Convert.ToInt32(dbParam[15].Value);
        }


        public int delete(string DesId)
        {
            DbParameter[] dbParam = new DbParameter[2];
            dbParam[0] = new DbParameter("@ID", DbParameter.DbType.Int, 1, Convert.ToInt32(DesId));
            dbParam[1] = new DbParameter("@ReturnVal", DbParameter.DbType.Int, 1, ParameterDirection.Output);
            DbConnectionDAL.ExecuteNonQuery(CommandType.StoredProcedure, "sp_Company_Information_del", dbParam);
            return Convert.ToInt32(dbParam[1].Value);
        }
    }
}

